package com.j2core.week01.helloworld;

/**
 * The application shows "Hello World!" on the screen.
 */
public class Main {

    public static void main(String[] args) {

        System.out.println("Hello World!");
    }
}
